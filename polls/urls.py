from django.urls import path
from django.contrib import admin
from . import views

app_name = 'polls'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),

]
admin.site.site_header = "Intership Admin"
admin.site.site_title = "Intership Admin Portal"
admin.site.index_title = "Welcome to Intership Researcher Portal"